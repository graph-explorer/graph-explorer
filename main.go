package main

import (
	"fmt"
	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"gitlab.com/graph-explorer/graph-explorer/resolver"
	"gitlab.com/graph-explorer/graph-explorer/schema"
	"gitlab.com/graph-explorer/graph-explorer/trace"
	"log"
	"net/http"
)

func main() {
	_, closer, err := trace.InitTracer()
	if err != nil {
		log.Fatal(err)
	}
	defer closer.Close()
	s := graphql.MustParseSchema(schema.GetSchema(), &resolver.QueryResolver{})
	fmt.Println("curl -XPOST -d '{\"query\": \"{ person {name, id, cpf} }\"}' localhost:8080/query")
	http.Handle("/query", &relay.Handler{Schema: s})
	log.Fatal(http.ListenAndServe(":8080", nil))

}
