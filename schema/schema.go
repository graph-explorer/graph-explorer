package schema

func GetSchema() string {
	return `
		schema {
			query: Query
		}

		type Query {
			person: [Person]
			organization: [Organization]
		}

		type Person  {
			id: String!
			cpf: String!
			name: String!
			associate: [Organization]
		}

		type Organization {
			id: String!
			cnpj: String!
			name: String!
			businessPartners: [Person]
		}
	`
}
