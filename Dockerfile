FROM golang:1.13

COPY . /api
WORKDIR /api

RUN go mod vendor
RUN go build
