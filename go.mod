module gitlab.com/graph-explorer/graph-explorer

require (
	github.com/graph-gophers/graphql-go v0.0.0-20190902214650-641ae197eec7
	github.com/graphql-go/graphql v0.7.8 // indirect
	github.com/opentracing/opentracing-go v1.1.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/uber/jaeger-client-go v2.17.0+incompatible
	github.com/uber/jaeger-lib v2.1.1+incompatible
)
