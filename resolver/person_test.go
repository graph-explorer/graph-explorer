package resolver

import "testing"

func getPersonResolver() PersonResolver {
	person := Person{
		Id:        "ID",
		Cpf:       "CPF",
		Name:      "Name",
		Associate: make([]*OrganizationResolver, 0, 0),
	}
	return PersonResolver{person: person}
}

func TestPersonId(t *testing.T) {
	resolver := getPersonResolver()
	if resolver.Id() != "ID" {
		t.Fatal("Invalid ID: " + resolver.Id())
	}
}

func TestPersonCpf(t *testing.T) {
	resolver := getPersonResolver()
	if resolver.Cpf() != "CPF" {
		t.Fatal("Invalid CPF: " + resolver.Cpf())
	}
}

func TestPersonName(t *testing.T) {
	resolver := getPersonResolver()
	if resolver.Name() != "Name" {
		t.Fatal("Invalid Name: " + resolver.Name())
	}
}

func TestPersonAssociate(t *testing.T) {
	resolver := getPersonResolver()
	associates, _ := resolver.Associate()
	if len(*associates) != 0 {
		t.Fatal("Invalid associates")
	}
}
