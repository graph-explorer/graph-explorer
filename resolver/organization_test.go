package resolver

import "testing"

func getOrganizationResolver() OrganizationResolver {
	organization := Organization{
		Id:               "ID",
		Cnpj:             "CNPJ",
		Name:             "Name",
		BusinessPartners: make([]*PersonResolver, 0, 0),
	}
	return OrganizationResolver{organization: organization}
}

func TestOrganizationId(t *testing.T) {
	resolver := getOrganizationResolver()
	if resolver.Id() != "ID" {
		t.Fatal("Invalid ID: " + resolver.Id())
	}
}

func TestOrganizationCnpj(t *testing.T) {
	resolver := getOrganizationResolver()
	if resolver.Cnpj() != "CNPJ" {
		t.Fatal("Invalid CNPJ: " + resolver.Cnpj())
	}
}

func TestOrganizationName(t *testing.T) {
	resolver := getOrganizationResolver()
	if resolver.Name() != "Name" {
		t.Fatal("Invalid Name: " + resolver.Name())
	}
}

func TestOrganizationBusinessPartners(t *testing.T) {
	resolver := getOrganizationResolver()
	partners, _ := resolver.BusinessPartners()
	if len(*partners) != 0 {
		t.Fatal("Invalid business partners")
	}
}
