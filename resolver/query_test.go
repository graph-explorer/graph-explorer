package resolver

import "testing"

func TestPerson(t *testing.T) {
	resolver := QueryResolver{}
	person, _ := resolver.Person()
	if len(*person) != 0 {
		t.Fatal("Invalid person")
	}
}

func TestOrganization(t *testing.T) {
	resolver := QueryResolver{}
	organization, _ := resolver.Organization()
	if len(*organization) != 0 {
		t.Fatal("Invalid organization")
	}
}
