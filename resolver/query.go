package resolver

type QueryResolver struct{}

func (_ *QueryResolver) Person() (*[]*PersonResolver, error) {
	resolvers := make([]*PersonResolver, 0, 0)
	return &resolvers, nil
}

func (_ *QueryResolver) Organization() (*[]*OrganizationResolver, error) {
	resolvers := make([]*OrganizationResolver, 0, 0)
	return &resolvers, nil
}
