package resolver

type Organization struct {
	Id               string
	Cnpj             string
	Name             string
	BusinessPartners []*PersonResolver
}

type OrganizationResolver struct {
	organization Organization
}

func (r *OrganizationResolver) Id() string {
	return r.organization.Id
}

func (r *OrganizationResolver) Cnpj() string {
	return r.organization.Cnpj
}

func (r *OrganizationResolver) Name() string {
	return r.organization.Name
}

func (r *OrganizationResolver) BusinessPartners() (*[]*PersonResolver, error) {
	return &r.organization.BusinessPartners, nil
}
