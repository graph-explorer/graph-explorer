package resolver

type Person struct {
	Id        string
	Cpf       string
	Name      string
	Associate []*OrganizationResolver
}

type PersonResolver struct {
	person Person
}

func (r *PersonResolver) Id() string {
	return r.person.Id
}

func (r *PersonResolver) Cpf() string {
	return r.person.Cpf
}

func (r *PersonResolver) Name() string {
	return r.person.Name
}

func (r *PersonResolver) Associate() (*[]*OrganizationResolver, error) {
	return &r.person.Associate, nil
}
