#!/bin/bash
# This file come from https://gitlab.com/majorhayden/os-containers

set -euxo pipefail

export STORAGE_DRIVER=vfs

# Newer versions of podman/buildah try to set overlayfs mount options when
# using the vfs driver, and this causes errors.
sed -i '/^mountopt =.*/d' /etc/containers/storage.conf

# Log into GitLab's container repository.
export REGISTRY_AUTH_FILE=${HOME}/auth.json
echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

# Set up the container's fully qualified name.
FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}"

# Build the container, squash it, and push it to the repository.
buildah bud -f Dockerfile -t ${IMAGE_NAME} .
CONTAINER_ID=$(buildah from ${IMAGE_NAME})
buildah commit --squash $CONTAINER_ID $FQ_IMAGE_NAME
buildah push ${FQ_IMAGE_NAME}
